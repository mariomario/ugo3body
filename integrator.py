#!/usr/bin/python
# -*- coding: latin-1 -*-
import math

G = 6.67*math.pow(10.0, -11.0) #gravitational constant

def a(v, x, t):
	return -x

#integrate until tmax
tmax = 10.0

#initial conditions
x = 1.0
v = 0.0
t = 0.0

print "t, x, v"
while t <= tmax: #the RK loop, finally
	print r, rho, P, t, x, v #we print the dimensional quantities first (radius, density, pressure) then the others
	#the RUNGE-KUTTA thing!
 	k1_x = v #estimate of the velocity
	k1_v = a(v, x, t) #let's use this to estimate acceleration
	k2_x = v + 0.5*h*k1_v #so we get a different estimate of the velocity
	k2_v = a(v + 0.5*h*k1_v, x + 0.5*h*k1_x, t + 0.5*h) #another estimate of acceleration
	k3_x = v + 0.5*h*k2_v #another estimate of velocity
	k3_v = a(v + 0.5*h*k2_v, x + 0.5*h*k2_x, t + 0.5*h) #you get the idea
	k4_x = v + h*k3_v #...
	k4_v = a(v + h*k3_v, x + h*k3_x, t + h) #...
	x = x + h*(k1_x + 2.0*k2_x + 2.0*k3_x + k4_x)/6.0 #putting them all together
	#RK magic is over
	rho = rho_c*math.pow(x, n) #dimensional density
	r = alpha*t #dimensional radius
	P = K*math.pow(rho, gammapoly) #dimensional pressure
	#update velocity and time
	v = v + h*(k1_v + 2.0*k2_v + 2.0*k3_v + k4_v)/6.0
	t = t + h

#We are done!

