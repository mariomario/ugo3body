m_1 <- 1e4 #G*milky way mass = G*10^10
m_2 <- 1 #G*GC mass = G*10^6
x_2 <- 1e4 #position of the GC center in pc
x_1 <- -m_2*x_2/m_1

m_1
m_2
x_1
x_2

logOmega <- 0.5*log(m_1+m_2) - 1.5*log(x_2 - x_1)
Omega <- exp(logOmega)
Omega

calculateJacobi <- function(x, y, z, vx, vy, vz)
{
	K <- 0.5*(vx*vx + vy*vy + vz*vz)
	U <- -0.5*Omega*Omega*(x*x + y*y) - m_1/r_1 - m_2/r_2
	return(K - U)
}

calculateJacobi()
