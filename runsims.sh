#!/bin/bash

for OMEGA in 0.01 0.002 
do
    cat template | sed s/'SUBSTITUTEOMEGA'/$OMEGA/g > t.c 
    gcc -o quinn t.c
    ./quinn > "21k$OMEGA.esc"
done