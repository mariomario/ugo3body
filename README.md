There are three R scripts and one c program.
Jacobi.R does not work (we would need no numerical integration at all if it did; I did not even try to make it work, because people prefer simulations, i.e. numerical integration to pen-and-paper calculations); o tempora o mores
quinn.R is based on Quinn 2010 method as described on page 3 of  https://arxiv.org/pdf/1103.1376.pdf it is an integrator for the motion in a tidal field on a circular orbit
verlet.R cannot be used with Coriolis acceleration so it is useless

quinn.c is the much faster C version of quinn.R <- USE THIS ONE
